
public class PassingCars {

	int getPairsOfCarPass(int[] arr) {
		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == 0) {
				count += getRightOneNumber(i + 1, arr);
			}

			if (arr[i] == 1) {
				count += getLeftZeroNumber(i, arr);
			}
		}
		int pairs = count / 2;
		return pairs > 1000000000 ? -1 : pairs;
	}

	int getRightOneNumber(int position, int[] arr) {

		int flag = 0;
		for (int i = position; i < arr.length; i++) {
			if (arr[i] == 1) {
				flag++;
			}
		}
		return flag;

	}

	int getLeftZeroNumber(int position, int[] arr) {
		int flag = 0;
		for (int i = 0; i < position; i++) {
			if (arr[i] == 0) {
				flag++;
			}
		}
		return flag;
	}

	public static void main(String[] args) {
		PassingCars pass = new PassingCars();
		int[] A = new int[] { 0, 1, 0, 1,0, 1 };
		System.out.println(pass.getPairsOfCarPass(A));
	}
}
