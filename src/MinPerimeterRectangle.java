
public class MinPerimeterRectangle {

	int getPerimeterRectangleMin(int N) {
		int x = ((Double) Math.sqrt(N)).intValue();
		for (int i = x; i > 0; i--) {
			if (N % i == 0) {
				int y = N/i;
				return 2 * (x + y);
			}
		}
		return 0;
	}

	public static void main(String[] args) {
		MinPerimeterRectangle a = new MinPerimeterRectangle();
		System.out.println(a.getPerimeterRectangleMin(30));
	}
}
