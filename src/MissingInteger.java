
public class MissingInteger {
	/*
	 * Utility function that puts all non-positive (0 and negative) numbers on left
	 * side of arr[] and return count of such numbers
	 */
	int segregate(int arr[]) {
		int size = arr.length;
		int j = 0;
		for (int i = 0; i < size; i++) {
			if (arr[i] <= 0) {
				int temp;
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				// increment count of non-positive
				// integers
				j++;
			}
		}

		return j;
	}

	/**
	 * Find the smallest positive missing number in an array that contains all
	 * positive integers
	 * @param positiveIntegers Array of positive integer
	 * @return
	 */
	int getSmallestMissingPositive(int[] positiveIntegers) {
		int[] positiveNumbers = getPositiveNumbers(positiveIntegers);
		int size = positiveNumbers.length;

		// Mark positiveIntegers[i] as visited by making positiveIntegers[positiveIntegers[i] - 1] negative. Note that 1 is
		// subtracted because index start
		// from 0 and positive numbers start from 1
		for (int i = 0; i < size; i++) {
			int x = Math.abs(positiveNumbers[i]);
			if (x - 1 < size && positiveNumbers[x - 1] > 0)
				positiveNumbers[x - 1] = -positiveNumbers[x - 1];
		}

		// Return the first index value at which
		// is positive
		for (int i = 0; i < size; i++)
			if (positiveNumbers[i] > 0)
				return i + 1; // 1 is added because indexes start from 0

		return size + 1;

	}

	/**
	 * Get positive numbers from arr
	 * @param arr Array of Integer
	 * @return Array of positive integer
	 */
	int[] getPositiveNumbers(int[] arr) {
		int sizeNegative = segregate(arr);
		int sizePositive = arr.length - sizeNegative;
		int[] positiveNumbers = new int[sizePositive];
		int j = 0;
		for (int i = sizeNegative; i < arr.length; i++) {
			positiveNumbers[j++] = arr[i];
		}
		return positiveNumbers;
	}

	public static void main(String... strings) {
		MissingInteger a = new MissingInteger();
		int b = a.getSmallestMissingPositive(new int[] { 2, 4, 5 });
		System.out.println(b);
	}
}
